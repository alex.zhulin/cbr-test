drop schema if exists zhulin_cbr;

create schema zhulin_cbr
character set utf8
collate UTF8_GENERAL_CI;

use zhulin_cbr;

/*==============================================================*/
/* Table: job_list                                              */
/*==============================================================*/
create table if not exists job_list
(
   id                   int not null auto_increment,
   lastname             varchar(20) not null,
   firstname            varchar(10) not null,
   jobplace             varchar(100) not null,
   jobaddress           varchar(100) not null,
   primary key (id)
);

/*==============================================================*/
/* Index: i_lastname_firtstname                                 */
/*==============================================================*/
create unique index i_lastname_firtstname on job_list
(
   lastname,
   firstname
);

/*==============================================================*/
/* Table: phone_book                                            */
/*==============================================================*/
create table phone_book
(
   id                   int not null auto_increment,
   lastname             varchar(20) not null,
   firstname            varchar(10) not null,
   workphone            varchar(20),
   mobilephone          varchar(20),
   mail                 varchar(100),
   birthday             date,
   jobplace             text not null,
   primary key (id)
);
