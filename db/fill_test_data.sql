use zhulin_cbr;

truncate table job_list;

insert into job_list
(lastname, firstname, jobplace, jobaddress)
values
('Иванов', 'Иван', 'Компания №1', 'Пермь, ул Ленина 10');

insert into job_list
(lastname, firstname, jobplace, jobaddress)
values
('Петров', 'Петр', 'Компания №1', 'Пермь, ул Ленина 10');

insert into job_list
(lastname, firstname, jobplace, jobaddress)
values
('Семенов', 'Семен', 'Компания №2', 'Пермь, ул Ленина 20');

insert into job_list
(lastname, firstname, jobplace, jobaddress)
values
('Дмитриев', 'Кирилл', 'Компания №2', 'Пермь, ул Ленина 20');


truncate table phone_book;

insert into phone_book
(lastname, firstname, workphone, mobilephone, mail, birthday, jobplace)
values
('Иванов', 'Иван', '932-123-45-67', '932-555-55-55', 'ivanov@mail.ru', '1972-01-01', 'Компания №1');
