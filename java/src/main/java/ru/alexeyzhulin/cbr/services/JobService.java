package ru.alexeyzhulin.cbr.services;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.alexeyzhulin.cbr.models.JobItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class JobService {
    private final JdbcTemplate jdbcTemplate;

    public JobService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<JobItem> getJobItems(String filter) {
        List<JobItem> jobItems = new ArrayList<>();

        String queryText = "select id, lastname, firstname, jobplace, jobaddress from job_list where lastname like ? order by lastname, firstname;";
        List<Map<String, Object>> recordList = jdbcTemplate.queryForList(queryText, (filter == null) ? "%" : (filter + "%"));
        for (Map<String, Object> record : recordList) {
            jobItems.add(new JobItem((int)record.get("id"),
                    (String)record.get("lastname"),
                    (String)record.get("firstname"),
                    (String)record.get("jobplace"),
                    (String)record.get("jobaddress")
                    ));
        }

        return jobItems;
    }

    @Transactional
    public void addJobItem(String lastName,
                           String firstName,
                           String jobPlace,
                           String jobAddress) {
        // Delete old data by unique index
        String queryText = "delete from job_list where lastname = ? and firstname = ?;";
        jdbcTemplate.update(queryText, lastName, firstName);
        // Insert the new one
        queryText = "insert into job_list (lastname, firstname, jobplace, jobaddress) values (?, ?, ?, ?);";
        jdbcTemplate.update(queryText, lastName, firstName, jobPlace, jobAddress);
    }
}
