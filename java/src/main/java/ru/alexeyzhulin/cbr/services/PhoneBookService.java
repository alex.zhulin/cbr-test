package ru.alexeyzhulin.cbr.services;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.alexeyzhulin.cbr.models.PhoneBookItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class PhoneBookService {
    private final JdbcTemplate jdbcTemplate;

    public PhoneBookService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<PhoneBookItem> getPhoneBookItems(String filter) {
        List<PhoneBookItem> phoneBookItems = new ArrayList<>();

        String queryText = "select id, lastname, firstname, workphone, mobilephone, mail, birthday, jobplace from phone_book where lastname like ? order by lastname, firstname;";
        List<Map<String, Object>> recordList = jdbcTemplate.queryForList(queryText, (filter == null) ? "%" : (filter + "%"));
        for (Map<String, Object> record : recordList) {
            phoneBookItems.add(new PhoneBookItem((int)record.get("id"),
                    (String)record.get("lastname"),
                    (String)record.get("firstname"),
                    (String)record.get("workphone"),
                    (String)record.get("mobilephone"),
                    (String)record.get("mail"),
                    (Date) record.get("birthday"),
                    (String)record.get("jobplace")
                    ));
        }

        return phoneBookItems;
    }
}
