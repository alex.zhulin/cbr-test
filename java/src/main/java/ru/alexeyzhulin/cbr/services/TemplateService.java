package ru.alexeyzhulin.cbr.services;

import org.springframework.stereotype.Component;
import ru.alexeyzhulin.cbr.models.TemplateParams;

@Component
public class TemplateService {
    public static final String JOB_LIST = "job_list";
    private static final String PHONE_BOOK = "phone_book";

    public TemplateParams getTemplateParams(String dictionaryType, String filterValue) {
        if (dictionaryType == null) {
            return  new TemplateParams("nav-link active", "nav-link", true, false, filterValue);
        }
        switch (dictionaryType) {
            case JOB_LIST:
                return new TemplateParams("nav-link active", "nav-link", true, false, filterValue);
            case PHONE_BOOK:
                return new TemplateParams("nav-link", "nav-link active", false, true, filterValue);
            default:
                return new TemplateParams("nav-link active", "nav-link", true, false, filterValue);
        }
    }
}
