package ru.alexeyzhulin.cbr.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ru.alexeyzhulin.cbr.services.JobService;
import ru.alexeyzhulin.cbr.services.PhoneBookService;
import ru.alexeyzhulin.cbr.services.TemplateService;

@Controller
public class ApplicationController {

    private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

    private final JobService jobService;
    private final PhoneBookService phoneBookService;
    private final TemplateService templateService;

    public ApplicationController(JobService jobService, PhoneBookService phoneBookService, TemplateService templateService) {
        this.jobService = jobService;
        this.phoneBookService = phoneBookService;
        this.templateService = templateService;
    }

    @RequestMapping("/")
    public String showItemList(Model model,
                               @RequestParam(value = "filter", required = false, defaultValue = "") String filter,
                               @RequestParam(value = "dictionary_type", required = false) String dictionaryType
    ) {
        log.info("[showItemList] Starting...");
        model.addAttribute("joblist", jobService.getJobItems(filter));
        model.addAttribute("phoneBook", phoneBookService.getPhoneBookItems(filter));
        model.addAttribute("templateParams", templateService.getTemplateParams(dictionaryType, filter));
        log.info("[showItemList] Done.");
        return "main";
    }

    @RequestMapping("job_new_record_1")
    public String jobNewRecord() {
        log.info("[jobNewRecord]");
        return "new-record";
    }

    @RequestMapping("job_new_record_2")
    public RedirectView jobNewRecordEnd(@RequestParam(value = "lastname") String lastName,
                                        @RequestParam(value = "firstname") String firstName,
                                        @RequestParam(value = "jobplace") String jobPlace,
                                        @RequestParam(value = "jobaddress") String jobAddress,
                                        RedirectAttributes attributes
    ) {
        log.info("[jobNewRecordEnd] Starting...");
        // Add new record
        jobService.addJobItem(lastName, firstName, jobPlace, jobAddress);
        // Redirect to home url
        attributes.addAttribute("dictionary_type", TemplateService.JOB_LIST);
        log.info("[jobNewRecordEnd] Done.");
        return new RedirectView("/");
    }
}
