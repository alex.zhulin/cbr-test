package ru.alexeyzhulin.cbr.models;

public class TemplateParams {
    private String jobListClass;
    private String phoneBookClass;
    private boolean showJobList;
    private boolean showPhoneBook;
    private String filterValue;

    public TemplateParams(String jobListClass, String phoneBookClass, boolean showJobList, boolean showPhoneBook, String filterValue) {
        this.jobListClass = jobListClass;
        this.phoneBookClass = phoneBookClass;
        this.showJobList = showJobList;
        this.showPhoneBook = showPhoneBook;
        this.filterValue = filterValue;
    }

    public String getJobListClass() {
        return jobListClass;
    }

    public String getPhoneBookClass() {
        return phoneBookClass;
    }

    public boolean isShowJobList() {
        return showJobList;
    }

    public boolean isShowPhoneBook() {
        return showPhoneBook;
    }

    public String getFilterValue() {
        return filterValue;
    }
}
