package ru.alexeyzhulin.cbr.models;

public class JobItem {
    private int id;
    private String lastName;
    private String firstName;
    private String jobPlace;
    private String jobAddress;

    public JobItem(int id, String lastName, String firstName, String jobPlace, String jobAddress) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.jobPlace = jobPlace;
        this.jobAddress = jobAddress;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getJobPlace() {
        return jobPlace;
    }

    public String getJobAddress() {
        return jobAddress;
    }
}
