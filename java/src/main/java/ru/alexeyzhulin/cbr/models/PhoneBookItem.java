package ru.alexeyzhulin.cbr.models;

import java.util.Date;

public class PhoneBookItem {
    private int id;
    private String lastName;
    private String firstName;
    private String workPhone;
    private String mobilePhone;
    private String mail;
    private Date birthDay;
    private String jobPlace;

    public PhoneBookItem(int id, String lastName, String firstName, String workPhone, String mobilePhone, String mail, Date birthDay, String jobPlace) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.workPhone = workPhone;
        this.mobilePhone = mobilePhone;
        this.mail = mail;
        this.birthDay = birthDay;
        this.jobPlace = jobPlace;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getMail() {
        return mail;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public String getJobPlace() {
        return jobPlace;
    }
}
